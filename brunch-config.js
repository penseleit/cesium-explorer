// See http://brunch.io for documentation.
module.exports = {
  files: {
    javascripts: {
      joinTo: {
        'vendor.js': /^vendor/, // Files that are not in `app` dir.
        'app.js': /^app\/scripts.*js/, 
      }
    },
    stylesheets: {joinTo: 'app.css'}
  },
  server: {
    port: 3334,
    hostname: '0.0.0.0'
  },
   plugins: {
      babel: {
        "presets": ["@babel/preset-env"]
      }
    }
}
  
  

  