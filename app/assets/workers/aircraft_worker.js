importScripts('http_worker.js');


onmessage = function(e){
  HTTPGet(e.data.url, e.data.options, HandleAircraftJSON);
}

onerror = function(error){

  let errorMessage = {
    type: 'Error',
    text: 'Error encountered: ' + error.message + ' on line ' + error.lineno + ' in file ' + error.filename
  }
  postMessage(errorMessage);
}

let aircraftCountry = {
  'United States': {
    name: 'United States',
    color: 'rgba(32, 150, 237, 1.0)',

  },
  'Germany': {
    name: 'Germany', 
    color: 'rgba(255, 150, 0, 1.0)',
  },
  'Australia': {
    name: 'Australia', 
    color: 'rgba(222, 0, 255, 1.0)',
  },
  'China': {
    name: 'China', 
    color: 'rgba(255, 42, 0, 1.0)',
  },
  'Canada': {
    name: 'Canada',
    color: 'rgba(255, 255, 255, 1.0)'
  }
};

function HandleAircraftJSON(jsonData){
  
  if (jsonData == undefined || jsonData == null){
    let errorMessage = {
      type: 'Error',
      text: 'Error encountered: No JSON data returned form HTTP request'
    }
    postMessage(errorMessage);
    return;
  }

  //Process jsonData
  let processedJSONData = ProcessAircraftJSON(jsonData); //make this a promise

  let successMessage = {
    type: 'Success',
    text: 'Fetching aircraft data from the OpenSky Network succeeded.',
    JSON: processedJSONData
  };
    
  postMessage(successMessage);
  
}

function ProcessAircraftJSON(aircraftJSON){

  let aircraftJSONObject = {};

  if(aircraftJSON != undefined && aircraftJSON != null){  
    
    aircraftJSON['states'].forEach(aircraft => {

      if(aircraft[6] != null && aircraft[5] != null){ //Only process aircraft with long and lat values
    
        //Determine source of position data
        let aPosSource = '';

        switch(aircraft[16]){
          case 0: aPosSource = 'ADS-B';
            break;
          case 1: aPosSource = 'ASTERIX';
            break;
          case 2: aPosSource = 'MLAT';
            break;  
        }

        //Set description for InfoBox
        let aDescription = `<h2>Aircraft ${aircraft[1]}</h2> 
        <table> 
            <tr> 
                <td>ICAO 24 Id</td><td>${aircraft[0]}</td> 
            </tr> 
            <tr> 
                <td>Callsign</td><td>${aircraft[1]}</td> 
            </tr> 
            <tr> 
                <td>Country of origin</td><td>${aircraft[2]}</td> 
            </tr> 
            <tr> 
                <td>Velocity</td><td>${aircraft[9]} m/s</td> 
            </tr> 
            <tr> 
                <td>Track</td><td>${aircraft[10]}&deg;</td> 
            </tr> 
            <tr> 
                <td>Vertical rate</td><td>${aircraft[11] ? aircraft[11] : 0} m/s</td> 
            </tr> 
            <tr> 
                <td>Altitude</td><td>${aircraft[13] ? aircraft[13] : 0 } m</td> 
            </tr> 
            <tr> 
                <td>Position source</td><td>${aPosSource}</td> 
            </tr> 
        </table>`
        
        //Determine image colour to use
        let aImage = 'images/default.gif';
        let aColor = 'rgba(0,0,0,1.0)';
          if(aircraftCountry[aircraft[2]]){
            aImage = 'images/' + aircraft[2] + '.gif';
            aColor = aircraftCountry[aircraft[2]].color;
        } 

        let aircraftData = {
          icao24: aircraft[0],
          name: aircraft[1],
          country: aircraft[2],
          long: aircraft[6],
          lat: aircraft[5],
          position: [aircraft[6], aircraft[5], aircraft[13]],
          description: aDescription,
          visible: false,
          updated: false,
          point_primitive: {
            show: true,
            pixelSize: 8,
            color: aColor,
            outlineColor: 'rgba(0, 0, 0, 1.0)',
            outlineWidth: 2,
            translucencyByDistance: [2e6, 1.0, 1e7, 0.6],
            scaleByDistance: [1e4, 1.0, 1e7, 0.2],
            id: aircraft[0],
            check: aircraft[6] + aircraft[5] + aircraft[13]
          },
          billboard: {
            show: true,
            image: aImage,
            scale: 0.7,
            rotation: aircraft[10],
            translucencyByDistance: [2e6, 1.0, 1e7, 0.6],
            scaleByDistance: [5e4, 1.0, 5e6, 0.25]
          }
        };

        aircraftJSONObject[aircraft[0]] = aircraftData; //Use ICAO 24 value as aircraft id in object
      }
    });
  }

  return aircraftJSONObject;
}


