function HTTPGet(url, options, callback){

    let request = new Request(url, options);
   
    fetch(request)
      .then(response => {
        if(response.status === 200) {
          //console.log('Fetch response status: 200');
         //console.log(response);
            return response.json();
        } else if (response.status === 304){
          //console.log('Fetch response status: 304');
          return response.json();
        }
        else {
            throw new Error ('Unexpected fetch response status: ' + response.statusText);
        }
      })
      .then(jsonResponse => {
        
        callback(jsonResponse);
  
      }).catch(error => {
        console.error('Error while fetching: ' + error);
      })
  }
  
  