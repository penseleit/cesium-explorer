const map = require('../viewmodel/map_viewmodel');
const nav_vm = require('../viewmodel/nav_viewmodel');

let dataSources = [];
let clusterLabelFont= '16px "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Helvetica Neue", Helvetica, Arial, sans-serif';


exports.Init = function(){
    let vwr = map.viewer();

    vwr.entities.owner.clustering.enabled = nav_vm.ToolsNavViewModel.enabled;
    vwr.entities.owner.clustering.minimumClusterSize = nav_vm.ToolsNavViewModel.minimumClusterSize;
    vwr.entities.owner.clustering.pixelRange = nav_vm.ToolsNavViewModel.pixelRange;

    vwr.entities.owner.clustering.clusterEvent.addEventListener(function(clusteredEntities, cluster) {
            
        try{
            let clusteredCount = clusteredEntities.length;
            cluster.label.fillColor = Cesium.Color.BLACK;
            cluster.label.text = clusteredCount < 200 ? clusteredCount.toString() : '> 200';
            cluster.label.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
            cluster.label.font = clusterLabelFont;
            cluster.label.fillColor = Cesium.Color.BLACK;
            cluster.label.show = true;
            cluster.label.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
            cluster.billboard.show = false;
            cluster.point.show = true;
            cluster.point.color = Cesium.Color.GREY;
            cluster.point.pixelSize = clusteredCount < 150 ? clusteredCount :  150 ;
            cluster.point.outlineColor = Cesium.Color.DARKGREY;
            cluster.point.outlineWidth = 2;
            cluster.point.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
            cluster.point.translucencyByDistance = new Cesium.NearFarScalar(1.5e2, 1.0, 15e8, 0.3);
            cluster.point.scaleByDistance = new Cesium.NearFarScalar(1.5e2, 1.0, 15e8, 0.3); 
    
            clusteredCount = null;
        }
        catch(err){
            console.log(err);
        }
    });
}

exports.AddCustomDataSource = function(name, clusteringEnabled, clusterPixelRange, minClusterSize, addClusteringEventListener){
    
    let dataSource = new Cesium.CustomDataSource(name);
    dataSource.clustering.enabled = clusteringEnabled;
    dataSource.clustering.pixelRange = clusterPixelRange;
    dataSource.clustering.minimumClusterSize = minClusterSize;

    if(addClusteringEventListener){

        dataSource.clustering.clusterEvent.addEventListener(function(clusteredEntities, cluster) {
            
            try{
                let clusteredCount = clusteredEntities.length;
                cluster.label.fillColor = Cesium.Color.BLACK;
                cluster.label.text = clusteredCount < 200 ? clusteredCount.toString() : '> 200';
                cluster.label.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
                cluster.label.font = clusterLabelFont;
                cluster.label.fillColor = Cesium.Color.BLACK;
                cluster.label.show = true;
                cluster.label.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
                cluster.billboard.show = false;
                cluster.point.show = true;
                cluster.point.color = Cesium.Color.LIGHTGREY;
                cluster.point.pixelSize = clusteredCount < 150 ? clusteredCount :  150 ;
                cluster.point.outlineColor = Cesium.Color.DARKGREY;
                cluster.point.outlineWidth = 2;
                cluster.point.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
                cluster.point.translucencyByDistance = new Cesium.NearFarScalar(1.5e2, 1.0, 15e8, 0.3);
                cluster.point.scaleByDistance = new Cesium.NearFarScalar(1.5e2, 1.0, 15e8, 0.3); 

                clusteredCount = null;
            }
            catch(err){
                console.log(err);
            }
        });
    }
        
    dataSources.push(dataSource);

    return dataSource;
}

exports.DataSources = dataSources;
