const map = require('../viewmodel/map_viewmodel');
const data = require('./data');
const msgmaker= require('../viewmodel/mapmessage_viewmodel');

let pollInterval = 15000;
let pollingAircraftWorker;
let httpWorker;
let pollingIntervalID;

exports.GetAllAircraft = function(){
    msgmaker.ShowMessage('<p>Getting aircraft positions from the OpenSky Network</p>', msgmaker.MapMessageType.INFO, 10000);

    let fetchMessage = {
        url: 'https://opensky-network.org/api/states/all',
        options:  {
            method: 'GET',
            mode: 'cors'
        }
    }

    SpawnAircraftHTTPWorker(fetchMessage).then((result) =>{
        //Save result to memory
        data.StoreAircraftData(result.JSON);
        if(typeof httpWorker !== 'undefined' && httpWorker !== null){
            console.log('Terminated spawned one time HTTP worker');
            httpWorker.terminate();
        }        
        //Send event so data can be processed
        map.MapEventTarget.dispatchEvent(new Event('aircraft_data_ready'));

        //SpawnPollingAircraftHTTPWorker(fetchMessage);

    }).catch(error => console.log(error));
    
}

exports.PollAllAircraft = function(){
    msgmaker.ShowMessage('<p>Getting aircraft positions from the OpenSky Network</p>',msgmaker.MapMessageType.INFO, 10000);

    let fetchMessage = {
        url: 'https://opensky-network.org/api/states/all',
        options:  {
            method: 'GET',
            mode: 'cors'
        },
        poll_interval: pollInterval
    }

    SpawnPollingAircraftHTTPWorker(fetchMessage);

}


exports.CancelPollingAllAircraft = function(){
    
    if(typeof pollingIntervalID !== 'undefined' && pollingIntervalID !== null){
        clearInterval(pollingIntervalID);
        
        pollingAircraftWorker.terminate();

        console.log('Aircraft data updates stopped.');
        msgmaker.ShowStatusMessage('Aircraft data updates stopped');
        
    }
}

function SpawnAircraftHTTPWorker(message){
    return new Promise((resolve, reject)=>{
        
        if (window.Worker){
            httpWorker = new Worker('workers/aircraft_worker.js');

            httpWorker.postMessage(message);

            httpWorker.onerror = function(error){
                reject(error);
            }

            httpWorker.onmessage = function(e){
                if(e.data !== null && e.data !== undefined){
                    resolve(e.data);
                }
                else{
                    reject(new Error('No data returned from API request.'));
                }
                
            }
        }
        else{
            reject(new Error('This browser does not support web workers. Unable to process HTTP request.'));
        }
    }).catch((error)=>{
        if(typeof httpWorker !== 'undefined' && httpWorker !== null){
            httpWorker.terminate();
        }
    });
}

function SpawnPollingAircraftHTTPWorker(message){

    if (window.Worker){
        pollingAircraftWorker = new Worker('workers/aircraft_worker.js');
    
        pollingIntervalID = setInterval(() => {
            
            pollingAircraftWorker.postMessage(message);
    
            pollingAircraftWorker.onerror = function(error){
                console.error('Polling aircraft HTTP Worker returned an error: ' + error);
            }
    
            pollingAircraftWorker.onmessage = function(e){
                if(e.data !== null && e.data !== undefined){
                    data.StoreAircraftData(e.data.JSON); //Save result to memory
                    map.MapEventTarget.dispatchEvent(new Event('aircraft_data_ready')); //Send event so data can be processed
                   
                    //msgmaker.ShowStatusMessage('Aircraft data updates received from OpenSky Network. Number of aircraft returned: <strong>' + Object.keys(e.data.JSON).length + '</strong>');
                }
                else{
                    console.warn('No data returned from polled OpenSky Network API request.');
                }
            }
        }, message.poll_interval);
    }
    else{
        console.error('This browser does not support web workers. Unable to process polled HTTP request.');
    }
}



