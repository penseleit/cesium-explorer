const utils = require('./utils');
const map = require('./map_viewmodel');

function MakeRandomObjects(num){
    //Polyline primitives
    let polyCollection = new Cesium.PolylineCollection();
    
    for (var count = 0; count < num; count++){

        let lat = utils.getRandomFloat(-90.0, 90.0);
        let long = utils.getRandomFloat(-180.0, 180.0);
        
        lat = lat.toPrecision(6);
        long = long.toPrecision(6);
        
        let mhund = 0.0005;


        let posStart = new Cesium.Cartesian3.fromDegrees(long, lat);
        let posNext = new Cesium.Cartesian3.fromDegrees(parseFloat(long) - parseFloat(mhund), parseFloat(lat) - parseFloat(mhund));
       
        let posFinish = new Cesium.Cartesian3.fromDegrees(parseFloat(long) + parseFloat(mhund), parseFloat(lat) - parseFloat(mhund));
        let posArray = [posStart, posNext, posFinish];

        polyCollection.add({
            show: true,
            width: 2,
            loop: true,
            material: Cesium.Material.fromType('Color',{color:  new Cesium.Color(1.0, 1.0, 0.0, 1.0)}),
            positions : posArray,
            id : {'Random polyline': count}
        }); 
      
        //console.log(posStart.toString());
        //console.log('Longitude:' + long + ' Latitude: ' + lat);
    } 
    
    map.viewer().scene.primitives.add(polyCollection);
   
    console.log(num + ' random primitives created.');
}

exports.MakeRandomObjects = MakeRandomObjects;

function MakeRandomDatSources(num, sourceNum){
    let aCount = 0;
    
    for (var d = 0; d < sourceNum; d++){
        let dataSource = new Cesium.CustomDataSource(num);
        dataSource.clustering.enabled = true;
        dataSource.clustering.pixelRange = 50;
        dataSource.clustering.minimumClusterSize = 10;
        dataSource.clustering.clusterEvent.addEventListener(function(clusteredEntities, cluster) {
            
            try{
                let clusteredCount = clusteredEntities.length;
                cluster.label.fillColor = Cesium.Color.BLACK;
                cluster.label.text = clusteredCount < 200 ? clusteredCount.toString() : '> 200';
                cluster.label.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
                cluster.label.font = clusterLabelFont;
                cluster.label.fillColor = Cesium.Color.BLACK;
                cluster.label.show = true;
                cluster.label.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
                cluster.billboard.show = false;
                cluster.point.show = true;
                cluster.point.color = Cesium.Color.ORANGE;
                cluster.point.pixelSize = clusteredCount < 150 ? clusteredCount :  150 ;
                cluster.point.outlineColor = Cesium.Color.ORANGE;
                cluster.point.outlineWidth = 2;
                cluster.point.style = Cesium.LabelStyle.FILL_AND_OUTLINE;
                cluster.point.translucencyByDistance = new Cesium.NearFarScalar(1e2, 1.0, 1e7, 0.3);
                cluster.point.scaleByDistance = new Cesium.NearFarScalar(1e2, 1.0, 1e7, 0.3); 

                clusteredCount = null;
            }
            catch(err){
                console.log(err);
            }
        });
    
        map.viewer().dataSources.add(dataSource);    
        
    }


}