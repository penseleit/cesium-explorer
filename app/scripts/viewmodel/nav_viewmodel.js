const map = require('./map_viewmodel');
const renderer = require('../model/aircraft');

let toolsnav_viewmodel = {
    rightValue: Cesium.knockout.observable(-300),
    pixelRange: Cesium.knockout.observable(30),
    minimumClusterSize: Cesium.knockout.observable(10),
    enabled: Cesium.knockout.observable(true),
    cancel_polling: function(){renderer.CancelPollingAllAircraft()},
}



function subscribeClusteringParameter(name) {
    toolsnav_viewmodel[name].subscribe(
    function(newValue) {
        let vwr = map.viewer();
        vwr.entities.owner.clustering[name] = newValue;
}); 
}

subscribeClusteringParameter('pixelRange');
subscribeClusteringParameter('minimumClusterSize');
subscribeClusteringParameter('enabled');  

let filternav_viewmodel = {
    leftValue: Cesium.knockout.observable('-300px'),
    vesselcount_oth: Cesium.knockout.observable(0),
    vesselindic_oth: Cesium.knockout.observable(0),
    vesselcount_us: Cesium.knockout.observable(0),
    vesselindic_us: Cesium.knockout.observable(0),
    vesselcount_can: Cesium.knockout.observable(0),
    vesselindic_can: Cesium.knockout.observable(0),
    vesselcount_chn: Cesium.knockout.observable(0),
    vesselindic_chn: Cesium.knockout.observable(0),
    vesselcount_ger: Cesium.knockout.observable(0),
    vesselindic_ger: Cesium.knockout.observable(0),
    vesselsinview: Cesium.knockout.observable(0)
}

exports.ToolsNavViewModel = toolsnav_viewmodel;

exports.FilterNavViewModel = filternav_viewmodel;

