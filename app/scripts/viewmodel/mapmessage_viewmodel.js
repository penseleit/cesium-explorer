let mapMessageViewModel = {
    visible: Cesium.knockout.observable(false),
    type: Cesium.knockout.observable('info'),
    message: Cesium.knockout.observable('Copyright &copy; Peter Enseleit.'),
    display_status_bar: Cesium.knockout.observable(false),
    status_bar_text: Cesium.knockout.observable(''),
    latlong_text: Cesium.knockout.observable('Lat: Long:'),
    height_text: Cesium.knockout.observable('Height above Earth: km')
}

exports.MapMessageViewModel = mapMessageViewModel;

const mapMessageType = {
    INFO: 'info',
    WARNING: 'warning',
    DANGER: 'danger'
}

exports.MapMessageType = mapMessageType;

exports.ShowMessage = function(html, type, timeout){
    let self = this;
    if(typeof timeout === 'undefined' || timeout === null || timeout === 0){
        timeout = 2000;
    }

    mapMessageViewModel.visible = true;
    mapMessageViewModel.type = type;
    mapMessageViewModel.message = html;

    let timeOutId = window.setTimeout(self.HideMessage, timeout);
}

exports.HideMessage = function(){
    
    mapMessageViewModel.visible = false;
    mapMessageViewModel.type = mapMessageType.INFO;
    mapMessageViewModel.message = '';
}

exports.ShowStatusMessage = function(message, timeout){
    let self = this;
    if(typeof timeout === 'undefined' || timeout === null || timeout === 0){
        timeout = 2000;
    }

    mapMessageViewModel.status_bar_text = message;
    mapMessageViewModel.display_status_bar = true;
    let timeOutId = window.setTimeout(self.HideStatusMessage, timeout);
}

exports.HideStatusMessage = function(){
    mapMessageViewModel.status_bar_text = '';
    mapMessageViewModel.display_status_bar = false;
}

exports.UpdateLatLong = function(html){
    mapMessageViewModel.latlong_text = html;
}

exports.UpdateHeight = function(html){
    mapMessageViewModel.height_text = html;
}
