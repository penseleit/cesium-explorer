const map = require('./map_viewmodel');
const nav_vm = require('./nav_viewmodel');
const data = require('../model/data');
const msgmaker = require('./mapmessage_viewmodel');

const pointFloor =5000;

let geoRectangle = new Cesium.RectangleGeometry({
    ellipsoid : Cesium.Ellipsoid.WGS84,
    rectangle : Cesium.Rectangle.fromDegrees(0.0, 0.0, 0.1, 0.1),
    vertexFormat : Cesium.PerInstanceColorAppearance.VERTEX_FORMAT,
    material : Cesium.Color.fromRandom({alpha : 1.0})
});

let AircraftViewModel = {
    name: 'Aircraft',
    position: 0,
    country: 'No Nation',
    description: 'Aircraft',
    isvisible: false,
    billboard : {
        show: true,
        image : 'images/default.gif',
        scale : 0.5,
        rotation: 0,
        translucencyByDistance: new Cesium.NearFarScalar(1e3, 1.0, 2e4, 0.3),
        scaleByDistance: new Cesium.NearFarScalar(1e3, 1.0, 2e4, 0.3),
        distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0.0, 2e4)
    },
    point: {
        show: true,
        pixelSize: 10,
        color: Cesium.Color.LIGHTGREY,
        outlineColor: Cesium.Color.BLACK,
        outlineWidth: 2,
        translucencyByDistance: new Cesium.NearFarScalar(1e3, 1.0, 2e4, 0.3),
        scaleByDistance: new Cesium.NearFarScalar(1e3, 1.0, 2e4, 0.3),
        distanceDisplayCondition: new Cesium.DistanceDisplayCondition(0.0, 2e4)
    },
    
}

let AircraftType = {
    'PAX': 'Passenger',
    'CRGO': 'Cargo',
    'HELO': 'Helicopter'
}

let AircraftCountry= {
    USA: 'United States', 
    GER: 'Germany', 
    AUS: 'Australia', 
    CHN: 'China', 
    CAN: 'Canada'
};

function GetCurrentViewRectangle(){
    let currentViewRectangle = Cesium.Camera.DEFAULT_VIEW_RECTANGLE;
    let vwr = map.viewer();

    //Determine SceneMode to calculate currently viewed rectangle
    if(vwr.scene.mode == Cesium.SceneMode.SCENE2D){
        
        let pixWidth = vwr.scene.canvas.clientWidth;
        let pixHeight = vwr.scene.canvas.clientHeight;   
        
        let top = vwr.scene.camera.pickEllipsoid(new Cesium.Cartesian2(pixWidth/2, 1));
        let latN = vwr.scene.globe.cartographicLimitRectangle.north;
        typeof top === 'undefined' ? true : latN = Cesium.Cartographic.fromCartesian(top).latitude;
        
        let bottom = vwr.scene.camera.pickEllipsoid(new Cesium.Cartesian2(pixWidth/2, pixHeight -1 ));
        let latS = vwr.scene.globe.cartographicLimitRectangle.south;
        typeof bottom === 'undefined' ? true :  latS = Cesium.Cartographic.fromCartesian(bottom).latitude;
        
        let left = vwr.scene.camera.pickEllipsoid(new Cesium.Cartesian2(1, pixHeight/2 + 20));
        let longW = Cesium.Cartographic.fromCartesian(left).longitude;
        let right = vwr.scene.camera.pickEllipsoid(new Cesium.Cartesian2(pixWidth -1, pixHeight/2));
        let longE = Cesium.Cartographic.fromCartesian(right).longitude;

        currentViewRectangle = new Cesium.Rectangle(longW, latS, longE, latN);
       
        top = null;
        bottom = null;
        right = null;
        left = null;
        pixWidth = null;
        pixHeight = null;

    } else if(vwr.scene.mode == Cesium.SceneMode.SCENE3D){

        currentViewRectangle = vwr.scene.camera.computeViewRectangle(vwr.scene.globe.ellipsoid);

    }  else {
        console.warn('ColumbusView is not supported.');
        return;
    }

    return currentViewRectangle;
}

exports.RenderAircraft = function(height){
    let aircraftJSON = data.GetAircraftData();
    Render(aircraftJSON, height);
}

function Render(airJSON, height){
    //msgmaker.ShowStatusMessage('Updating aircraft positions');

    let currentViewRectangle = GetCurrentViewRectangle();

    let vwr = map.viewer();

    let geoAircraft;

    if(typeof currentViewRectangle !== 'undefined' && currentViewRectangle !== null){
        try{
            
            if(typeof vwr.scene.primitives === 'undefined' || vwr.scene.primitives === null){
                vwr.scene.primitives.add(new Cesium.PointPrimitiveCollection());
            }
            if(vwr.scene.primitives.length === 0){
                vwr.scene.primitives.add(new Cesium.PointPrimitiveCollection());
            }


            vwr.entities.suspendEvents();

            let aCountries = {};
            let aCount = 0;
            let primitiveCollection;
            let entityCollection;
            let idsToRemove = [];

            if (height < pointFloor){
   
                entityCollection = vwr.entities;
                
                //Get entities to remove
                for(var i = 0; i < entityCollection.values.length; i++){
                    let airCart = Cesium.Cartographic.fromCartesian(entityCollection.values[i].position.getValue(Cesium.JulianDate.now()));
                    let isWithinViewRectangle = Cesium.Rectangle.contains(currentViewRectangle, airCart);
                    if(! isWithinViewRectangle){
                        idsToRemove.push(entityCollection.values[i].id);
                    }
                }

                //Add entities
                for(var aircraft in airJSON){
                    let airObject = airJSON[aircraft];
                    let airPos = Cesium.Cartesian3.fromDegrees(airObject.position[1], airObject.position[0], airObject.position[2]);
                    let airCart = Cesium.Cartographic.fromCartesian(airPos);
                    let isWithinViewRectangle = Cesium.Rectangle.contains(currentViewRectangle, airCart);
                    if(isWithinViewRectangle){
                        entityCollection.add(MakeEntity(airObject));
                    }
                    
                }
               
                //Remove entities not in current view
                for(var x = 0; x < idsToRemove.length; x++){
                    entityCollection.removeById(idsToRemove[x]);
                }

                
                 for(var i = 0; i < entityCollection.values.length; i++){
                    if(aCountries[entityCollection.values[i].country]){
                        aCountries[entityCollection.values[i].country] += 1;
                    }else{
                        aCountries[entityCollection.values[i].country] = 1;
                    }
                }

                aCount = entityCollection.values.length;

                 //Remove points
                 vwr.scene.primitives.get(0).removeAll();

                 
            }
            else{
               
                //Render points
                vwr.scene.primitives.get(0).removeAll();
                primitiveCollection = vwr.scene.primitives.get(0);
                /* let pColl;
                if(vwr.scene.primitives.length <= 1){
                    pColl = vwr.scene.primitives.add(new Cesium.PrimitiveCollection({show: true, destroyPrimitives: true}));
                }
                vwr.scene.primitives.get(1).removeAll();
                pColl = vwr.scene.primitives.get(1); */

                for(var aircraft in airJSON){
                    let airObject = airJSON[aircraft];
                    let airPos = Cesium.Cartesian3.fromDegrees(airObject.position[1], airObject.position[0], airObject.position[2]);
                    let airCart = Cesium.Cartographic.fromCartesian(airPos);
                    let isWithinViewRectangle = Cesium.Rectangle.contains(currentViewRectangle, airCart);
                    if(isWithinViewRectangle){
                        primitiveCollection.add(MakePoint(airObject));
                        //pColl.add(MakeGeometry(airObject));    
                        
                        if(aCountries[airObject.country]){
                            aCountries[airObject.country] += 1;
                        }else{
                            aCountries[airObject.country] = 1;
                        }
                    }
                    
                }
                
                aCount = primitiveCollection.length;
              
                //Remove entities
                vwr.entities.removeAll();
            }

            PopulateVesselCounters(aCountries, aCount);
            
        }
        catch(error){
            console.log('Error rendering aircraft. ' + error);
        }
        finally{
            vwr.entities.resumeEvents();
        }
    }
   
} 

function PopulateVesselCounters(countries, count){

    try{
        //Populate vessel counters
        let usCount = countries[AircraftCountry.USA] ? countries[AircraftCountry.USA] : 0;
        let gerCount = countries[AircraftCountry.GER] ? countries[AircraftCountry.GER] : 0;
        let canCount = countries[AircraftCountry.CAN] ? countries[AircraftCountry.CAN] : 0;
        let chnCount = countries[AircraftCountry.CHN] ? countries[AircraftCountry.CHN] : 0;
        let othCount = count - chnCount - canCount - gerCount - usCount;

        let vcus = 0;
        (usCount > 0 && count > 0) ? vcus = (usCount/count)*100 : vcus = 0;
        nav_vm.FilterNavViewModel.vesselcount_us = vcus;
        nav_vm.FilterNavViewModel.vesselindic_us = usCount;
        let vcger = 0;
        (gerCount > 0 && count > 0) ? vcger = (gerCount/count)*100 : vcger = 0;
        nav_vm.FilterNavViewModel.vesselcount_ger = vcger;
        nav_vm.FilterNavViewModel.vesselindic_ger = gerCount;
        let vccan = (canCount/count)*100;
        (canCount > 0 && count > 0) ? vccan = (canCount/count)*100 : vccan = 0;
        nav_vm.FilterNavViewModel.vesselcount_can = vccan;
        nav_vm.FilterNavViewModel.vesselindic_can = canCount;
        let vcchn = (chnCount/count)*100;
        (chnCount > 0 && count > 0) ? vcchn = (chnCount/count)*100 : vcchn = 0;
        nav_vm.FilterNavViewModel.vesselcount_chn = vcchn;
        nav_vm.FilterNavViewModel.vesselindic_chn = chnCount;
        let vcoth = (othCount/count)*100;
        (othCount > 0 && count > 0) ? vcoth = (othCount/count)*100 : vcoth = 0;
        nav_vm.FilterNavViewModel.vesselcount_oth = vcoth;
        nav_vm.FilterNavViewModel.vesselindic_oth = othCount;
        nav_vm.FilterNavViewModel.vesselsinview = count;
    }
    catch(error){
        console.log('Error counting aircraft countries: ' + error);
    }
}

function MakePoint(airObject){

    let airPosition = Cesium.Cartesian3.fromDegrees(airObject.position[1], airObject.position[0], airObject.position[2]);
    let airTranslucency = new Cesium.NearFarScalar(airObject.point_primitive.translucencyByDistance[0], airObject.point_primitive.translucencyByDistance[1], airObject.point_primitive.translucencyByDistance[2], airObject.point_primitive.translucencyByDistance[3]);
    let airScale = new Cesium.NearFarScalar(airObject.point_primitive.scaleByDistance[0], airObject.point_primitive.scaleByDistance[1], airObject.point_primitive.scaleByDistance[2], airObject.point_primitive.scaleByDistance[3]);
    
    let newPoint = {
            show: airObject.point_primitive.show,
            color: new Cesium.Color.fromCssColorString(airObject.point_primitive.color),
            id: airObject.point_primitive.id,
            outlineColor: new Cesium.Color.fromCssColorString(airObject.point_primitive.color),
            pixelSize: airObject.point_primitive.pixelSize,
            position: airPosition,
            translucencyByDistance: airTranslucency,
            scaleByDistance: airScale,
            check: airObject.point_primitive.check
    }

    airPosition = null;
    airTranslucency = null
    airScale = null;

    return newPoint;
}

function MakeEntity(airObject){

    let airPosition = Cesium.Cartesian3.fromDegrees(airObject.position[1], airObject.position[0], airObject.position[2]);
    let scratch = new Cesium.Cartesian3();
    let p2 = Cesium.Cartesian3.multiplyByScalar(airPosition, 0.0001, scratch);
    let p3 = Cesium.Cartesian3.multiplyByScalar(airPosition, -0.0002, scratch);
    
    let airTranslucency = new Cesium.NearFarScalar(airObject.billboard.translucencyByDistance[0], airObject.billboard.translucencyByDistance[1], airObject.billboard.translucencyByDistance[2], airObject.billboard.translucencyByDistance[3]);
    let airScale = new Cesium.NearFarScalar(airObject.billboard.scaleByDistance[0], airObject.billboard.scaleByDistance[1], airObject.billboard.scaleByDistance[2], airObject.billboard.scaleByDistance[3]);
    let airRotation = Cesium.Math.toRadians(360 - airObject.billboard.rotation);
   
    let newEntity = {
        id: airObject.ica024,
        country: airObject.country,
        name: airObject.name,
        show: airObject.billboard.show,
        description: airObject.description,
        position: airPosition,
        billboard: {
            image: airObject.billboard.image,
            show: airObject.billboard.show,
            scale: airObject.billboard.scale,
            rotation: airRotation,
            translucencyByDistance: airTranslucency,
            scaleByDistance: airScale,
        }, 
      

    }

    airPosition = null;
    airTranslucency = null
    airScale = null;
    airRotation = null;

    return newEntity;
}

function MakeGeometry(airObject){
    let airPosition = Cesium.Cartesian3.fromDegrees(airObject.position[1], airObject.position[0], airObject.position[2]);
    let newM = new Cesium.Matrix4();
    let posMatrix = Cesium.Matrix4.multiplyByTranslation(Cesium.Matrix4.IDENTITY, airPosition, newM);
    let geoInstance = new Cesium.GeometryInstance({
        geometry : geoRectangle,
        modelMatrix: posMatrix, 
        id : airObject.id,
        attributes : {
            //color : new Cesium.ColorGeometryInstanceAttribute.toValue(Cesium.Color.fromCssColorString(airObject.point_primitive.color))
            color : new Cesium.ColorGeometryInstanceAttribute.fromColor(Cesium.Color.GREEN)
            
          }
      });
      
    
    let newGeometry = new Cesium.Primitive({
        geometryInstances : geoInstance,
        appearance : new Cesium.PerInstanceColorAppearance({
            translucent: false
        })
        

    }); 

    return newGeometry;
}


