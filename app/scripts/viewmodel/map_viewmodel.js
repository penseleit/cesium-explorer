//The base module used to initialise the map, controls and data

const map = require('../view/map_view');
const mapControls = require('../view/map_controls_view');
const dataSources = require('../model/datasources');
const aircraft = require('../model/aircraft');
const msgmaker = require('./mapmessage_viewmodel');

let viewer;

exports.MakeMap = function(){
    return new Promise((resolve, reject)=>{
        
        viewer = map.Init();
        
        if(typeof viewer !== 'undefined' && viewer !== null){
            resolve(viewer);
        }
        else{
            reject('Cesium Viewer object returned as undefined or null.');
        }

    }).then((value)=>{

        try{
            mapControls.Init();
            dataSources.Init();
            aircraft.GetAllAircraft();
        }
        catch(error){
            throw error;
        }

        return value;

    }).catch((error)=>{
        console.log('Error, unable to create map: '+ error);
        msgmaker.ShowMessage('<strong>Error creating map</strong> <p>Error, unable to create map: </p><p>'+ error + '</p>');
    });

}

exports.viewer = function(){
    return viewer;
}

exports.MapEventTarget = map.MapEventTarget;