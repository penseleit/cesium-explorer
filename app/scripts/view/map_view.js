
const msgmaker = require('../viewmodel/mapmessage_viewmodel');
const utils = require('../viewmodel/utils');
const aircraft_vm = require('../viewmodel/aircraft_viewmodel');
const nav_vm = require('../viewmodel/nav_viewmodel');

//Setup Cesium map
let viewer;
let mapEventTarget = new EventTarget();
     

exports.Init = function(){

    let homeExtent = Cesium.Rectangle.fromDegrees(120, -5, 150, -45);
    Cesium.Camera.DEFAULT_VIEW_RECTANGLE = homeExtent;
  
    viewer = new Cesium.Viewer('mapContainer', {
        animation: false,
        timeline: false,
        navigationInstructionsInitiallyVisible: false,
        sceneMode: Cesium.SceneMode.SCENE2D,
        imageryProvider: Cesium.createOpenStreetMapImageryProvider(),
        baseLayerPicker: false,
        contextOptions: {
            webgl : {
               alpha : false,
               depth : true,
               stencil : false,
               antialias : false, // This one is the only modified to false from true, the rest are at default values
               premultipliedAlpha : true,
               preserveDrawingBuffer : false,
               failIfMajorPerformanceCaveat : true
            },
            allowTextureFilterAnisotropic : false // this should improve a little bit
        } 
    });

    //Improve framerate
    viewer.scene.fxaa = false; 

  
     //Add cesium-navigation plugin
    viewer.extend(Cesium.viewerCesiumNavigationMixin, {
        defaultResetView: homeExtent,      
        enableCompass: true,
        enableZoomControls: true,
        enableDistanceLegend: true,
        enableCompassOuterRing: true
      });

    
    //Add event listeners
    mapEventTarget.addEventListener('aircraft_data_ready', aircraft_vm.RenderAircraft);
    viewer.scene.camera.moveEnd.addEventListener(HandleCameraChange);
    viewer.scene.canvas.addEventListener('mousemove', UpdateLatLong);
    
    viewer.scene.camera.flyHome();

    return viewer;
 
}

function UpdateLatLong(e){

    let pos = viewer.camera.pickEllipsoid(new Cesium.Cartesian3(e.clientX, e.clientY));
    
    if (typeof pos !== 'undefined' && pos !== null) {
        let cartoPos = Cesium.Cartographic.fromCartesian(pos);
        let long = Cesium.Math.toDegrees(cartoPos.longitude).toFixed(10);
        let lat = Cesium.Math.toDegrees(cartoPos.latitude).toFixed(10);
        let ew = ' W';
        let ns = ' N';
        let latDMS = utils.DegreesToDMS(lat);
        let longDMS = utils.DegreesToDMS(long);
     
        if(lat < 0.0){ 
            ns = ' S'; 
            latDMS = latDMS.substring(1)
        } 
     
        if(long < 0.0){
            ew = ' E'; 
            longDMS = longDMS.substring(1)
        }
      
        msgmaker.UpdateLatLong('<strong>Lat:</strong> ' + latDMS + ns + ' <strong>Long:</strong> ' + longDMS + ew );
    }
}

function HandleCameraChange(){
    
    let cameraHeight =0;
    
    if(viewer.scene.mode === Cesium.SceneMode.SCENE2D || viewer.scene.mode === Cesium.SceneMode.COLUMBUS_VIEW){
        cameraHeight = viewer.scene.camera.getMagnitude();
    }
    else if (viewer.scene.mode === Cesium.SceneMode.SCENE3D){
        cameraHeight = viewer.scene.globe.ellipsoid.cartesianToCartographic(viewer.scene.camera.position).height;
    }

    if(typeof cameraHeight !== 'undefined' && cameraHeight !== null){
        
        let heightAboveEarthinKM = (cameraHeight/1000).toFixed(2); 
        msgmaker.UpdateHeight('Height above Earth: <strong>' + heightAboveEarthinKM + ' km</strong>' );
        
        let fixedHeightAboveEarthInMetres = cameraHeight.toFixed(0);
        SetClusterPixelRange(fixedHeightAboveEarthInMetres);

        aircraft_vm.RenderAircraft(heightAboveEarthinKM);
    }
}

function SetClusterPixelRange(height){
    
    //Make cluster range smaller as height above earth increases
    //Min height = 1600m. Max height = 10,000,000m
    //Min pixelRange = 5. Max pixelRange used to calculate slope = 50
    //m = slope = y2-y2/x2-x1 = 5-50/10,000,000-1600
    let x1 = 10000000; //Max height above earth
    let x= height; //Measured current camera height above earth
    x > x1 ? x = x1 : true; //So that anything above this height pixelRange stays the same
    let y1 = 5; //Min pixelRange
    //let m = -9.5015202432389182269163066090574e-6; //Slope for 5-100 pixelRange
    let m = -4.5007201152184349495919347095535e-6 //Slope for 5-50 pixelRange
    let newPixelRange = y1 + (m*(x-x1));
    
    nav_vm.ToolsNavViewModel.pixelRange = newPixelRange;
}


exports.MapEventTarget = mapEventTarget;