//Setup controls on Cesium Viewer widget
const map = require('../viewmodel/map_viewmodel');
const mapmsg_vm = require('../viewmodel/mapmessage_viewmodel');
const nav_vm = require('../viewmodel/nav_viewmodel');

const nav_hide_value = '-300px'; //refer to css for what this value should be
const nav_show_value = '10px';

exports.Init = function(){
 
  SetupTitle();
  SetupSidebars();
  SetupMessageContainers();

  //Show feather icons
  feather.replace();
}

function SetupTitle(){

    //Add app title
    let appTitleElement = document.createElement('div');
    appTitleElement.id = 'apptitle';
    appTitleElement.classList.add('apptitle');
    appTitleElement.innerHTML = '<h1>Cesium Explorer</h1><span>Version 0.1</span';
  
    let csmViewer = document.getElementsByClassName('cesium-viewer');
    csmViewer[0].appendChild(appTitleElement);
}

function SetupSidebars(){
  
  //Add filters button to map
  let filtersButton = document.createElement('button');
  
  filtersButton.onclick = openFiltersNav;
  filtersButton.className = 'cesium-button cesium-toolbar-button';
  filtersButton.title = 'Show Filters';
  filtersButton.innerHTML = '<i style="padding-top: 2px;" data-feather="filter"></i>';
  filtersButton.style = 'cursor:pointer;';

  //Add tool button to map
  let toolsButton = document.createElement('button');
  
  toolsButton.onclick = openToolsNav;
  toolsButton.className = 'cesium-button cesium-toolbar-button';
  toolsButton.title = 'Show Tools';
  toolsButton.innerHTML = '<i style="padding-top: 1px;" data-feather="settings"></i>';
  toolsButton.style = 'cursor:pointer;';

  let toolBar = document.getElementsByClassName('cesium-viewer-toolbar')[0];
  toolBar.insertBefore(filtersButton, toolBar.childNodes[1]);
  toolBar.appendChild(toolsButton);

  //Add close click event listeners and data binding to sidebars
    let toolsnav = document.getElementById('toolsnav');
    Cesium.knockout.track(nav_vm.ToolsNavViewModel);
    Cesium.knockout.applyBindings(nav_vm.ToolsNavViewModel, toolsnav);
    
    let tCross = document.getElementById('tools-cross');
    tCross.addEventListener('click', closeToolsNav);

    let filtersnav = document.getElementById('filtersnav');
    Cesium.knockout.track(nav_vm.FilterNavViewModel);
    Cesium.knockout.applyBindings(nav_vm.FilterNavViewModel, filtersnav);

    let fCross = document.getElementById('filters-cross');
    fCross.addEventListener('click', closeFiltersNav);

}

function SetupMessageContainers(){

  //Add Message div to map bottomContainer and data bindings
  let mapMessageContainer = document.createElement('div');

  mapMessageContainer.classList.add('map-message');
  mapMessageContainer.id = 'map-message';
  mapMessageContainer.setAttribute('data-bind','css: {"danger-message": type === "danger", "warning-message": type === "warning", "info-message": type === "info", "visible": visible === true, "hidden": visible === false}');

  let mapMessageCross = document.createElement('a');
  mapMessageCross.className = 'close-cross map-message-close-cross';
  mapMessageCross.innerHTML = '&times;';
  mapMessageCross.id = 'map-message-close-cross';
  mapMessageCross.addEventListener('click', closeMapMessage);

  let mapMessageInnerDiv = document.createElement('div');
  mapMessageInnerDiv.setAttribute('data-bind', 'html: message');

  mapMessageContainer.appendChild(mapMessageCross);
  mapMessageContainer.appendChild(mapMessageInnerDiv);

  let msgContainer = map.viewer().bottomContainer;
  msgContainer.appendChild(mapMessageContainer); 

  //Add status bar container for simple info display
  let statusBarContainer = document.createElement('div');
  statusBarContainer.classList.add('map_statusbar');
  statusBarContainer.id = 'map_statusbar';
  statusBarContainer.setAttribute('data-bind', 'css: {display: display_status_bar}, html: status_bar_text');

  msgContainer.appendChild(statusBarContainer); 

  //Add Lat/Long container
  let latLongContainer = document.createElement('div');
  latLongContainer.classList.add('map_latlong');
  latLongContainer.id = 'map_latlong';
  latLongContainer.setAttribute('data-bind', 'html: latlong_text');

  msgContainer.appendChild(latLongContainer); 

  //Add Height container
  let heightContainer = document.createElement('div');
  heightContainer.classList.add('map_height');
  heightContainer.id = 'map_height';
  heightContainer.setAttribute('data-bind', 'html: height_text');

  msgContainer.appendChild(heightContainer); 


  Cesium.knockout.track(mapmsg_vm.MapMessageViewModel);
  Cesium.knockout.applyBindings(mapmsg_vm.MapMessageViewModel, msgContainer);

}

//Control Event Handlers
function openToolsNav() {
  nav_vm.ToolsNavViewModel.rightValue = nav_show_value;
}

function closeToolsNav() {
  nav_vm.ToolsNavViewModel.rightValue = nav_hide_value;
}

function openFiltersNav() {  
  nav_vm.FilterNavViewModel.leftValue = nav_show_value;    
}

function closeFiltersNav() {  
  nav_vm.FilterNavViewModel.leftValue = nav_hide_value;
}

function closeMapMessage(){
  mapmsg_vm.HideMessage();
}


